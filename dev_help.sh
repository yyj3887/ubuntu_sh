dev_if=dev_if
dev_firefox=dev_firefox
dev_deb=dev_deb
dev_apt=dev_apt
dev_dpkg=dev_dpkg
dev_service=dev_service
dev_port_set=dev_port_set

dev_install_ifconfig=dev_install_ifconfig
dev_install_git=dev_install_git
dev_install_node=dev_install_node
dev_install_npm=dev_install_npm
dev_install_pm2=dev_install_pm2
dev_install_mysql=dev_install_mysql

dev_start_blog_frontend=dev_start_blog_frontend
dev_start_blog_backend=dev_start_blog_backend
dev_start_sh=dev_start_sh

dev_git_url_blog=dev_git_url_blog

dev_local_set_mysql=dev_local_set_mysql

dev_sql_root_excute=dev_sql_root_excute
dev_sql_root_arter=dev_sql_root_arter
if [ $1 = HELP ]
then
    echo ===============리눅스 명령어 ==================
    echo 
    echo $dev_if"                "sh파일 조건문 사용법
    echo 
    echo $dev_firefox"           "sh파일 파이어폭스 URL 명령어 사용법
    echo
    echo $dev_deb"               ".deb 파일 설치 하는법
    echo
    echo $dev_apt"               "apt 명령 설명서
    echo
    echo $dev_dpkg"              "리눅스 현재 패키지 설치 리스트 확인법
    echo
    echo $dev_service"           "리눅스 백그라운드 서비스 확인
    echo
    echo $dev_service"           "리눅스 백그라운드 서비스 확인
    echo
    echo $dev_port_set"           "리눅스 포트 개방 외부접속 허용
    echo
    echo ===============개발 툴 설치 ==================
    echo 
    echo $dev_install_ifconfig"                "ifconfig 설치 net-tools ip 확인
    echo 
    echo $dev_install_git"                     "git 설치
    echo
    echo $dev_install_node"                    "nodejs 설치
    echo
    echo $dev_install_npm"                     "npm 설치
    echo
    echo $dev_install_pm2"                     "npm pkg pm2 서버 백그라운드 실행 매니저
    echo
    echo $dev_install_mysql"                   "mysql 설치 및 설정
    echo ===============프로젝트 리스트==================
    echo 
    echo $dev_start_blog_frontend"                    "블로그 개발 vuejs frontend
    echo 
    echo $dev_start_blog_backend"                     "블로그 개발 nodejs express backend
    echo
    echo $dev_start_sh"                               "리눅스 sh 파일 수정      
    echo
    echo ===============Git 저장소 리스트==================
    echo 
    echo $dev_git_url_blog"                           "루나 방울이 블로그 개발 git clone url    
    echo            
    echo
    echo ===============리눅스 로컬 셋팅 저장소==================
    echo 
    echo $dev_local_set_mysql"                           "mysql setting 경로 port,ip   
    echo            
    echo 
    echo ===============my sql 사용법==================
    echo 
    echo $dev_sql_root_excute"                           "mysql 콘솔 실행
    echo $dev_sql_root_arter"                            "mysql ER_NOT_SUPPORTED_AUTH_MODE error
    echo            
    echo    
fi

if [ $dev_port_set = $1 ]
then
    echo 아웃바운드 추가
    echo sudo iptables -I OUTPUT 1 -p tcp --dport 3000 -j ACCEPT
    echo
    echo 인바운드 추가
    echo sudo iptables -I INPUT 1 -p tcp --dport 3000 -j ACCEPT
    echo 
    echo 아웃바운드 삭제
    echo sudo iptables -D OUTPUT 1 -p tcp --dport 3000 -j ACCEPT
    echo
    echo 인바운드 삭제
    echo sudo iptables -D INPUT 1 -p tcp --dport 3000 -j ACCEPT
    echo
    echo iptables 리스트 보기 -L 리스트 -v 자세히
    echo sudo iptables -L -v
    echo
fi

if [ $dev_sql_root_excute = $1 ]
then
    echo
    sudo /usr/bin/mysql -u root -p
    echo 
fi

if [ $dev_sql_root_arter = $1 ]
then
    echo    
    echo ==Error==
    echo ER_NOT_SUPPORTED_AUTH_MODE
    echo ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '새로운 비밀번호';
    echo flush privileges;
    echo 
fi

if [ $dev_local_set_mysql = $1 ]
then
    echo
    echo /etc/mysql
    echo 
fi

if [ $dev_install_mysql = $1 ]
then
    echo
    echo mysql install ...
    sudo apt-get install mysql-server    
    echo
    echo mysql setting ...
    echo
    echo done.
fi

if [ $dev_service = $1 ]
then
    echo
    echo 전체 서비스 확인
    echo service --status-all
    echo
    echo + : 실행 중인 서비스
    echo - : 중지된 서비스
    echo
fi

if [ $dev_start_sh = $1 ]
then
    code /home/yyj/utill/ubuntu_sh
fi

if [ $dev_git_url_blog = $1 ]
then
    echo git clone https://gitlab.com/yyj3887/docs.git
fi

if [ $dev_install_pm2 = $1 ]
then
    echo npm repo... pm2 install...sudo npm install -g pm2
    sudo npm install -g pm2
    echo done.
fi

if [ $dev_start_blog_frontend = $1 ]
then
    code /home/yyj/dev_workspace/docs/frontend2/docs
fi

if [ $dev_start_blog_backend = $1 ]
then
    code /home/yyj/dev_workspace/docs/backend
fi


if [ $dev_install_ifconfig = $1 ]
then
    echo
    echo net tools Install...
    sudo apt install net-tools
    echo done.
fi

if [ $dev_install_git = $1 ]
then
    echo
    echo git Install...
    sudo apt install git
    echo done.
fi

if [ $dev_install_node = $1 ]
then
    echo
    echo nodejs Install...
    sudo apt install nodejs
    echo done.
fi

if [ $dev_install_npm = $1 ]
then
    echo
    echo nodejs Install...
    sudo apt install npm
    echo done.
fi

if [ $dev_if = $1 ]
then
    echo
    echo if [ 비교대상1 = 비교대상2 ]
    echo then
    echo fi
    echo 각 연사자에선 띄어쓰기가 중요!!!
    echo 연산자 설명 URL devkuma.com/books/pages/1178
    echo
fi

if [ $dev_firefox = $1 ]
then
    echo
    echo firefox [경로]
    echo
fi

if [ $dev_deb = $1 ]
then
    echo
    echo 데비안 소프트웨어 패키지
    echo
    echo sudo dpkg -i [파일].deb
    echo    
fi

if [ $dev_apt = $1 ]
then
echo
echo 어드밴스트 패키징 툴 advanced Packaging Tool,APT
echo
echo apt 명령"                      "기존 명령"                   "설명
echo apt install"                "apt-get install"             "패키지 목록
echo apt remove"                 "apt-get remove"              "패키지 삭제
echo apt purge"                  "apt-get purge"               "패키지와 관련 설정 제거
echo apt update"                 "apt-get update"              "레파지토리 인덱스 갱신
echo apt upgrade"                "apt-get upgrade"             "업그레이드 가능한 모든 패키지 업그레이드
echo apt autoremove"             "apt-get autoremove"          "불필요한 패키지 제거
echo apt full-upgrade"           "apt-get dist-upgrade"        "의존성 고려한 패키지 업그레이드
echo apt search"                 "apt-cache search"            "프로그램 검색
echo apt show"                   "apt-cache show"              "패키지 상세 정보 출력
echo apt list"                   "apt-get install
fi

if [ $dev_dpkg = $1 ]
then
echo
echo 전체 패키지 설치목록
echo dpkg --get-selections
echo
echo 원하는 패키지만 설치목록 볼때
echo dpkg --get-selections | grep [패키지]
echo
fi